### Eventos

- Oficina no 15º Congresso Internacional da Abraji, 13.set.2020: [Python para jornalistas](https://gitlab.com/rodolfo-viana/eventos/-/tree/main/20200913_abraji_analisededadoscompython)

- Open Class no IDP, 2.mar.2021: [Introdução à raspagem de dados](https://gitlab.com/rodolfo-viana/eventos/-/tree/main/20210302_idp_webscraping)

- Workshop no Google Developers Group de Foz do Iguaçu, 27.mar.2021: [Webscraping com Python](https://gitlab.com/rodolfo-viana/eventos/-/tree/main/20210327_gdgfoz_webscrapingcompython)
